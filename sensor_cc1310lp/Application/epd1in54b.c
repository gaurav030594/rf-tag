/**
 *  @filename   :   epd1in54b.c
 *  @brief      :   Implements for Dual-color e-paper library
 *  @author     :   Yehui from Waveshare
 *
 *  Copyright (C) Waveshare     July 13 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documnetation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to  whom the Software is
 * furished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <stdlib.h>
#include "epd1in54b.h"
#include "epdif.h"
#include "imagedata.h"

int EPD_Init(EPD* epd) {
    unsigned int i = 0;
  epd->reset_pin = RST_PIN;
  epd->dc_pin = DC_PIN;
  epd->cs_pin = CS_PIN;
  epd->busy_pin = BUSY_PIN;
  epd->width = EPD_WIDTH;
  epd->height = EPD_HEIGHT;
  
  /* this calls the peripheral hardware interface, see epdif */
  if (EpdInitCallback() != 0) {
    return -1;
  }
#if 0
  for(i=0;i<2;i++) {
       EPD_DigitalWrite(epd,TEST_PIN, LOW);
       sleep(1);
       EPD_DigitalWrite(epd,TEST_PIN, HIGH);
       sleep(1);
   }
#endif
  /* EPD hardware init start */
  EPD_Reset(epd);
#if 0
  EPD_SendCommand(epd, POWER_SETTING);
  EPD_SendData(epd, 0x07);
  EPD_SendData(epd, 0x00);
  EPD_SendData(epd, 0x08);
  EPD_SendData(epd, 0x00);
  EPD_SendCommand(epd, BOOSTER_SOFT_START);
  EPD_SendData(epd, 0x07);
  EPD_SendData(epd, 0x07);
  EPD_SendData(epd, 0x07);
  EPD_SendCommand(epd, POWER_ON);
  EPD_WaitUntilIdle(epd);

  EPD_SendCommand(epd, PANEL_SETTING);
  EPD_SendData(epd, 0xcf);
  EPD_SendCommand(epd, VCOM_AND_DATA_INTERVAL_SETTING);
  EPD_SendData(epd, 0x17);
  EPD_SendCommand(epd, PLL_CONTROL);
  EPD_SendData(epd, 0x39);
  EPD_SendCommand(epd, TCON_RESOLUTION);
  EPD_SendData(epd, 0xC8);
  EPD_SendData(epd, 0x00);
  EPD_SendData(epd, 0xC8);
  EPD_SendCommand(epd, VCM_DC_SETTING_REGISTER);
  EPD_SendData(epd, 0x0E);

  EPD_SetLutBw(epd);
  EPD_SetLutRed(epd);
#endif
#if 0
  EPD_SendCommand(epd, SW_RESET);

  EPD_SendCommand(epd, SET_ANALOG_BLOCK_CTRL);
  EPD_SendData(epd, 0x54);
  EPD_SendCommand(epd, SET_DIGITAL_BLOCK_CTRL);
  EPD_SendData(epd, 0x3B);
  EPD_SendCommand(epd, DRIVER_OUTPUT_CTRL);
  EPD_SendData(epd, 0xD3);
  EPD_SendData(epd, 0x00);
  EPD_SendData(epd, 0x00);

  EPD_SendCommand(epd, DATE_ENTRY_MODE_SETTING);
  EPD_SendData(epd, 0x01);

  EPD_SendCommand(epd, SET_RAM_X_ADDR_START_END_POS);
  EPD_SendData(epd, 0x00);
  EPD_SendData(epd, 0x0F);

  EPD_SendCommand(epd, SET_RAM_Y_ADDR_START_END_POS);
  EPD_SendData(epd, 0xD3);
  EPD_SendData(epd, 0x00);
  EPD_SendData(epd, 0x00);
  EPD_SendData(epd, 0x00);

  EPD_SendCommand(epd, TEMP_SENSOR_CTRL);
  EPD_SendData(epd, 0x07);
  EPD_SendData(epd, 0xFF);

  EPD_SendCommand(epd, DISPLAY_UPDATE_CTRL_2);
  EPD_SendData(epd, 0x91);

  EPD_SendCommand(epd, MASTER_ACTIVATION);

  EPD_WaitUntilIdle(epd);

#endif
#if 1
  EPD_SendCommand(epd, SW_RESET);

    EPD_SendCommand(epd, SET_ANALOG_BLOCK_CTRL);
    EPD_SendData(epd, 0x54);
    EPD_SendCommand(epd, 0x75);
    EPD_SendData(epd, 0x3B);


    EPD_SendCommand(epd, DRIVER_OUTPUT_CTRL);
    EPD_SendData(epd, 0x27);
    EPD_SendData(epd, 0x01);
    EPD_SendData(epd, 0x00);


    EPD_SendCommand(epd, SET_DUMMY_LINE_PERIOD);
    EPD_SendData(epd, 0x35);

    EPD_SendCommand(epd, SET_GATE_LINE_WIDTH);
    EPD_SendData(epd, 0x04);

    EPD_SendCommand(epd, DATE_ENTRY_MODE_SETTING);
    EPD_SendData(epd, 0x03);

    EPD_SendCommand(epd, SET_RAM_X_ADDR_START_END_POS);
    EPD_SendData(epd, 0x00);
    EPD_SendData(epd, 0x0F);

    EPD_SendCommand(epd, SET_RAM_Y_ADDR_START_END_POS);
    EPD_SendData(epd, 0x00);
    EPD_SendData(epd, 0x00);
    EPD_SendData(epd, 0x27);
    EPD_SendData(epd, 0x01);

    EPD_SendCommand(epd, SOURCE_DRIVING_VOLTAGE_CTRL);
    EPD_SendData(epd, 0x41);
    EPD_SendData(epd, 0xA8);
    EPD_SendData(epd, 0x32);

    EPD_SendCommand(epd, WRITE_VCOM_REG);
    EPD_SendData(epd, 0x68);

    EPD_SendCommand(epd, BORDER_WAVEFORM_CTRL);
    EPD_SendData(epd, 0x33);

#endif
    WRITE_LUT(epd);
  /* EPD hardware init end */

  return 0;
}

/**
 *  @brief: this calls the corresponding function from epdif.h
 *          usually there is no need to change this function
 */
void EPD_DigitalWrite(EPD* epd, int pin, int value) {
  EpdDigitalWriteCallback(pin, value);
}

/**
 *  @brief: this calls the corresponding function from epdif.h
 *          usually there is no need to change this function
 */
int EPD_DigitalRead(EPD* epd, int pin) {
  return EpdDigitalReadCallback(pin);
}

/**
 *  @brief: this calls the corresponding function from epdif.h
 *          usually there is no need to change this function
 */
void EPD_DelayMs(EPD* epd, unsigned int delaytime) {  // 1ms
  EpdDelayMsCallback(delaytime);
}

/**
 *  @brief: basic function for sending commands
 */
void EPD_SendCommand(EPD* epd, unsigned char command) {
  SCLK_H;
  EPD_DigitalWrite(epd,DC_PIN, LOW);
  EPD_DigitalWrite(epd,CS_PIN, HIGH);
  EpdSpiTransferCallback(command);
  EPD_DigitalWrite(epd,DC_PIN, HIGH);
}

/**
 *  @brief: basic function for sending data
 */
void EPD_SendData(EPD* epd, unsigned char data) {
  //SCLK_H;
  //EPD_DigitalWrite(epd,DC_PIN, HIGH);
  //EPD_DigitalWrite(epd, CS_PIN, HIGH);
  EpdSpiTransferCallback(data);
}

/**
 *  @brief: Wait until the busy_pin goes HIGH
 */
void EPD_WaitUntilIdle(EPD* epd) {
  while(EPD_DigitalRead(epd, epd->busy_pin) == 1) {      //1: busy, 0: idle
    EPD_DelayMs(epd, 100);
  }      
}

/**
 *  @brief: module reset.
 *          often used to awaken the module in deep sleep,
 *          see EPD::Sleep();
 */
void EPD_Reset(EPD* epd) {
  EPD_DigitalWrite(epd,RST_PIN, LOW);                //module reset
  EPD_DelayMs(epd, 200);
  EPD_DigitalWrite(epd,RST_PIN, HIGH);
  EPD_DelayMs(epd, 200);    
}

const unsigned char init_data[]={
     0x22,   0x11,   0x10,   0x00,   0x10,   0x00,   0x00,   0x11,   0x88,   0x80,   0x80,   0x80,   0x00,   0x00,   0x6A,   0x9B,
     0x9B,   0x9B,   0x9B,   0x00,   0x00,   0x6A,   0x9B,   0x9B,   0x9B,   0x9B,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,
     0x00,   0x00,   0x00,   0x04,   0x18,   0x04,   0x16,   0x01,   0x0A,   0x0A,   0x0A,   0x0A,   0x02,   0x00,   0x00,   0x00,
     0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x04,   0x04,   0x08,   0x3C,   0x07,   0x00,   0x00,   0x00,   0x00,
     0x00,   0x00,   0x00,   0x00,   0x00,   0x00,
};

void WRITE_LUT(EPD* epd)
{
    unsigned char i;
    EPD_SendCommand(epd, WRITE_LUT_REGISTER);
    for(i=0;i<70;i++)           // write LUT register with 29bytes instead of 30bytes 2D13
        EPD_SendData(epd,init_data[i]);
}
#if 0
/**
 *  @brief: set the look-up tables
 */
void EPD_SetLutBw(EPD* epd) {
  unsigned int count;     
  EPD_SendCommand(epd, 0x20);         //g vcom
  for(count = 0; count < 15; count++) {
    EPD_SendData(epd, lut_vcom0[count]);
  } 
  EPD_SendCommand(epd, 0x21);        //g ww --
  for(count = 0; count < 15; count++) {
    EPD_SendData(epd, lut_w[count]);
  } 
  EPD_SendCommand(epd, 0x22);         //g bw r
  for(count = 0; count < 15; count++) {
    EPD_SendData(epd, lut_b[count]);
  } 
  EPD_SendCommand(epd, 0x23);         //g wb w
  for(count = 0; count < 15; count++) {
    EPD_SendData(epd, lut_g1[count]);
  } 
  EPD_SendCommand(epd, 0x24);         //g bb b
  for(count = 0; count < 15; count++) {
    EPD_SendData(epd, lut_g2[count]);
  } 
}

void EPD_SetLutRed(EPD* epd) {
  unsigned int count;     
  EPD_SendCommand(epd, 0x25);
  for(count = 0; count < 15; count++) {
    EPD_SendData(epd, lut_vcom1[count]);
  } 
  EPD_SendCommand(epd, 0x26);
  for(count = 0; count < 15; count++) {
    EPD_SendData(epd, lut_red0[count]);
  } 
  EPD_SendCommand(epd, 0x27);
  for(count = 0; count < 15; count++) {
    EPD_SendData(epd, lut_red1[count]);
  } 
}
#endif
void set_xy_window(EPD* epd,unsigned char xs,unsigned char xe,unsigned int ys,unsigned int ye)
{
    EPD_SendCommand(epd,SET_RAM_X_ADDR_START_END_POS);       // set RAM x address start/end, in page 36
    EPD_SendData(epd,xs);        // RAM x address start at 00h;
    EPD_SendData(epd,xe);        // RAM x address end at 0fh(12+1)*8->104
    EPD_SendCommand(epd,SET_RAM_Y_ADDR_START_END_POS);       // set RAM y address start/end, in page 37
    EPD_SendData(epd,ys);        // RAM y address start at 0;
    EPD_SendData(epd,ys>>8);
    EPD_SendData(epd,ye);        // RAM y address end at
    EPD_SendData(epd,ye>>8);     // RAM y address end at
}
void set_xy_counter(EPD* epd,unsigned char x,unsigned int y)
{
    EPD_SendCommand(epd,SET_RAM_X_ADDR_COUNTER);       // set RAM x address count
    EPD_SendData(epd,x);
    EPD_SendCommand(epd,SET_RAM_Y_ADDR_COUNTER);       // set RAM y address count
    EPD_SendData(epd,y);
    EPD_SendData(epd,y>>8);
}
void Display_update(EPD* epd)
{
    EPD_SendCommand(epd,DISPLAY_UPDATE_CTRL_2);
    EPD_SendData(epd,0xC7);      //Load LUT from MCU(0x32), Display update
    //EPD_SendData(epd,0x91);      //Load LUT from OTP
    EPD_SendCommand(epd,MASTER_ACTIVATION);
    usleep(1000);
    EPD_WaitUntilIdle(epd);
}

void EPD_DisplayFrame(EPD* epd, const unsigned char* frame_buffer_black, const unsigned char* frame_buffer_red)
{
    unsigned int i = 0;
    unsigned char temp = 0;
    unsigned int bit = 0;
#if 0
    set_xy_window(epd,0, (EPD_WIDTH/8)-1, 0, EPD_HEIGHT-1);
    set_xy_counter(epd,0,0);
    EPD_SendCommand(epd,WRITE_RAM_BW);

    for (i = 0; i < epd->width * epd->height / 8; i++) {
      temp = 0x00;
      for (bit = 0; bit < 4; bit++) {
        if ((frame_buffer_black[i] & (0x80 >> bit)) != 0) {
          temp |= 0xC0 >> (bit * 2);
        }
      }
      EPD_SendData(epd, temp);
      temp = 0x00;
      for (bit = 4; bit < 8; bit++) {
        if ((frame_buffer_black[i] & (0x80 >> bit)) != 0) {
          temp |= 0xC0 >> ((bit - 4) * 2);
        }
      }
      EPD_SendData(epd, temp);
    }
#endif
    set_xy_window(epd,0, (EPD_WIDTH/8)-1, 0, EPD_HEIGHT-1);
    set_xy_counter(epd,0,0);
    EPD_SendCommand(epd,WRITE_RAM_BW);
    for (i = 0; i < epd->width * epd->height / 8; i++) {
      EPD_SendData(epd, frame_buffer_black[i]);
    }
#if 1
    set_xy_window(epd,0, (EPD_WIDTH/8)-1, 0, EPD_HEIGHT-1);
    set_xy_counter(epd,0,0);
    EPD_SendCommand(epd,WRITE_RAM_RED);
    for (i = 0; i < epd->width * epd->height / 8; i++) {
      EPD_SendData(epd, ~frame_buffer_red[i]);
    }
#endif
    Display_update(epd);

}

void EPD_Sleep(EPD *epd)
{
    EPD_SendCommand(epd, DEEP_SLEEP_MODE);
    EPD_SendData(epd, 0x01);
}
const unsigned char lut_vcom0[] =
{
  0x0E, 0x14, 0x01, 0x0A, 0x06, 0x04, 0x0A, 0x0A,
  0x0F, 0x03, 0x03, 0x0C, 0x06, 0x0A, 0x00
};

const unsigned char lut_w[] =
{
  0x0E, 0x14, 0x01, 0x0A, 0x46, 0x04, 0x8A, 0x4A,
  0x0F, 0x83, 0x43, 0x0C, 0x86, 0x0A, 0x04
};

const unsigned char lut_b[] = 
{
  0x0E, 0x14, 0x01, 0x8A, 0x06, 0x04, 0x8A, 0x4A,
  0x0F, 0x83, 0x43, 0x0C, 0x06, 0x4A, 0x04
};

const unsigned char lut_g1[] = 
{
  0x8E, 0x94, 0x01, 0x8A, 0x06, 0x04, 0x8A, 0x4A,
  0x0F, 0x83, 0x43, 0x0C, 0x06, 0x0A, 0x04
};

const unsigned char lut_g2[] = 
{
  0x8E, 0x94, 0x01, 0x8A, 0x06, 0x04, 0x8A, 0x4A,
  0x0F, 0x83, 0x43, 0x0C, 0x06, 0x0A, 0x04
};

const unsigned char lut_vcom1[] = 
{
  0x03, 0x1D, 0x01, 0x01, 0x08, 0x23, 0x37, 0x37,
  0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

const unsigned char lut_red0[] = 
{
  0x83, 0x5D, 0x01, 0x81, 0x48, 0x23, 0x77, 0x77,
  0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

const unsigned char lut_red1[] = 
{
  0x03, 0x1D, 0x01, 0x01, 0x08, 0x23, 0x37, 0x37,
  0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};


/* END OF FILE */


