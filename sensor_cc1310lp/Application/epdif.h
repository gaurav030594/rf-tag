/**
 *  @filename   :   epdif.h
 *  @brief      :   Header file of epdif.c providing EPD interface functions
 *                  Users have to implement all the functions in epdif.cpp
 *  @author     :   Yehui from Waveshare
 *
 *  Copyright (C) Waveshare     July 7 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documnetation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to  whom the Software is
 * furished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef EPDIF_H
#define EPDIF_H

//#include "stm32f1xx_hal.h"

#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>
#include <ti/drivers/GPIO.h>
#include <ti/drivers/gpio/GPIOCC26XX.h>
#include "unistd.h"

#define DIO10_SCLK                  IOID_10
#define DIO09_MOSI                  IOID_9
#define DIO07_CS                    IOID_7
#define DIO15_DC                    IOID_15
#define DIO14_RES                   IOID_14
#define DIO13_BSY                   IOID_13
//#define DIO06_TEST                  IOID_6

static PIN_State GPIOPinState;
static PIN_Handle GPIOPinHandle;

#define SCLK_H      PIN_setOutputValue(GPIOPinHandle, DIO10_SCLK,1)
#define SCLK_L      PIN_setOutputValue(GPIOPinHandle, DIO10_SCLK,0)
#define SDA_H       PIN_setOutputValue(GPIOPinHandle, DIO09_MOSI,1)
#define SDA_L       PIN_setOutputValue(GPIOPinHandle, DIO09_MOSI,0)

// Pin definition
#define CS_PIN           0
#define RST_PIN          1
#define DC_PIN           2
#define BUSY_PIN         3
#define TEST_PIN        4

// Pin level definition
#define LOW             0
#define HIGH            1

typedef struct {
  int pin;
} EPD_Pin;

int  EpdInitCallback(void);
void EpdDigitalWriteCallback(int pin, int value);
int  EpdDigitalReadCallback(int pin);
void EpdDelayMsCallback(unsigned int delaytime);
void EpdSpiTransferCallback(unsigned char data);

#endif /* EPDIF_H */
