/*
 * unistd.h
 *
 *  Created on: 23-Nov-2018
 *      Author: gaurav
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "sys/types.h"

 unsigned sleep(unsigned seconds);
 int usleep(useconds_t useconds);

#ifdef __cplusplus
}
#endif


