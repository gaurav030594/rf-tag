/*
 * business_logic.c
 *
 *  Created on: 22-Sep-2018
 *      Author: balaji
 */




#include "unistd.h"

/* Driver Header files */
#include <ti/drivers/Power.h>
#include <ti/drivers/power/PowerCC26XX.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>
#include <ti/drivers/timer/GPTimerCC26XX.h>
//#include <ti/drivers/power/PowerCC26XX.h>
//#include <ti/drivers/Power.h>
#include "CC1310_LAUNCHXL.h"
 //#include <ti/sysbios/family/arm/cc26xx/Power.h>
 //#include <ti/sysbios/family/arm/cc26xx/PowerCC2650.h>
 //Power_setDependency(XOSC_HF);
//#incldue <ti/drivers/timer/GPTimerCC26XX.h>

/* Example/Board Header files */
#include "Board.h"


#include "epd1in54b.h"
#include "epdif.h"
#include "epdpaint.h"


#define TIMER_LOAD_VALUE_100MS      (4799999)
#define COLORED                     1
#define UNCOLORED                   0
/* Pin driver handles */
static PIN_Handle SevenSegDispPinHandle;
static PIN_State SevenSegDispPinState;
PIN_Config SevenSegmentPinTable[] = {
    IOID_18 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    IOID_19 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    IOID_20 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    IOID_21 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    IOID_22 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    IOID_23 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    IOID_24 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    IOID_26 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};


static PIN_Handle ButtonPinHandle;
static PIN_State ButtonPinState;

PIN_Config ButtonPinTable[] = {
    IOID_1  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    IOID_2  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    IOID_3  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    IOID_6  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    IOID_28  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    IOID_29  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    IOID_11  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
    PIN_TERMINATE
};


GPTimerCC26XX_Handle hTimer;
//volatile unsigned char display_refresh = 0;
unsigned int PinCode = 0;
volatile unsigned int Display_Val = 0;
volatile unsigned int timer_count = 0;
enum key_state {
    PLUS_KEY,
    MINUS_KEY,
    OK_KEY,
    ONE_KEY,
    TWO_KEY,
    THREE_KEY,
    FOUR_KEY,
    NO_KEY
}state;
enum system_state {
    PIN_ENTRY,
    QTY_ENTRY
}system_state;


Paint paint_black;
Paint paint_red;

EPD epd;
static unsigned char frame_buffer_black[EPD_WIDTH * EPD_HEIGHT / 8] = {0};
static unsigned char frame_buffer_red[EPD_WIDTH * EPD_HEIGHT / 8] = {0};


void timerCallback(GPTimerCC26XX_Handle handle, GPTimerCC26XX_IntMask interruptMask) {
    // interrupt callback code goes here. Minimize processing in interrupt.
    //PIN_setOutputValue(SevenSegDispPinHandle,IOID_6,!PIN_getOutputValue(IOID_6));
    //display_refresh = 1;
    timer_count++;
    if(timer_count == 20) {
        timer_count = 0;
        PinCode = 0;
    }
}
void buttonCallbackFxn(PIN_Handle handle, PIN_Id pinId) {
    /* Debounce logic, only toggle if the button is still pushed (low) */
    CPUdelay(8000*50);
    if (!PIN_getInputValue(pinId)) {
        /* Toggle LED based on the button pressed */
        switch (pinId) {
            case IOID_1:
                //+-BUTTON - OK
                state = PLUS_KEY;
                break;
            case IOID_2:
                //--BUTTON - OK
                state = MINUS_KEY;
                break;
            case IOID_3:
                //OK-BUTTON - OK
                state = OK_KEY;
                break;
            case IOID_6:
                //4-BUTTON - OK
                state = FOUR_KEY;
                break;
            case IOID_28:
                //1-BUTTON - OK
                state = ONE_KEY;
                break;
            case IOID_29:
                //2-BUTTON - OK
                state = TWO_KEY;
                break;
            case IOID_11:
                //3-BUTTON - OK
                state = THREE_KEY;
                break;
            default:
                /* Do nothing */
                break;
        }
    }
}
void timer_setup(void);
void display_digit(unsigned char pos, unsigned int value);
void write_SevenSegment_Data(unsigned int value);
void UpdateEPD(char *Message1,char *Message2,char *Message3);
void *business_logic(void *arg0)
{

    unsigned char digit_0 = 0;
    unsigned char digit_1 = 0;
    unsigned char digit_2 = 0;
    unsigned char digit_3 = 0;
    unsigned int Quantity = 0;

    SevenSegDispPinHandle = PIN_open(&SevenSegDispPinState, SevenSegmentPinTable);
    if(!SevenSegDispPinHandle) {

        while(1);   /* Error initializing board LED pins */
    }
    ButtonPinHandle = PIN_open(&ButtonPinState, ButtonPinTable);
    if(!ButtonPinHandle) {
        /* Error initializing board LED pins */
        while(1);
    }
    if (PIN_registerIntCb(ButtonPinHandle, &buttonCallbackFxn) != 0) {
        /* Error registering button callback function */
        while(1);
    }
    GPTimerCC26XX_Params params;

    GPTimerCC26XX_Params_init(&params);

    params.width          = GPT_CONFIG_16BIT;
    params.mode           = GPT_MODE_PERIODIC_UP;
    params.debugStallMode = GPTimerCC26XX_DEBUG_STALL_OFF;

    hTimer = GPTimerCC26XX_open(CC1310_LAUNCHXL_GPTIMER0A, &params);
    if(hTimer == NULL) {
       //Log_error0("Failed to open GPTimer");
       //Task_exit();
        while(1);
    }

 //   Types_FreqHz  freq;
 //   BIOS_getCpuFreq(&freq);
    //GPTimerCC26XX_Value loadVal = freq.lo / 1000 - 1; //47999
    GPTimerCC26XX_Value loadVal = (TIMER_LOAD_VALUE_100MS*10); //47999
    GPTimerCC26XX_setLoadValue(hTimer, loadVal);
    GPTimerCC26XX_registerInterrupt(hTimer, timerCallback, GPT_INT_TIMEOUT);


    if (EPD_Init(&epd) != 0) {
      return -1;
    }


    Paint_Init(&paint_black, frame_buffer_black, epd.width, epd.height);
    Paint_Init(&paint_red, frame_buffer_red, epd.width, epd.height);
    Paint_Clear(&paint_black, UNCOLORED);
#if 0
    Paint_Clear(&paint_red, UNCOLORED);
    Paint_DrawStringAt(&paint_red, 15, 111, "TONETAG", &Font24, COLORED);
    Paint_DrawStringAt(&paint_red, 10, 25, "PIN ENTRY", &Font20, COLORED);
    EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);
    EPD_Sleep(&epd);
    sleep(5);
#endif
    UpdateEPD("SENSOR","TI15.4","Receiving");

    sleep(10);
    system_state=PIN_ENTRY;
    while(1) {
        //write_SevenSegment_Data(9876);
        //usleep(1);
#if 0
        if(display_refresh == 1) {
            display_refresh = 0;
            write_SevenSegment_Data(Display_Val);
        }
#endif
        if(system_state==PIN_ENTRY) {
            if(state==ONE_KEY) {
                digit_0++;
                digit_0 = digit_0%10;
                PinCode = (digit_3*1000)+(digit_2*100)+(digit_1*10)+(digit_0*1);
                state = NO_KEY;

                GPTimerCC26XX_stop(hTimer);
                GPTimerCC26XX_Value loadVal = (TIMER_LOAD_VALUE_100MS*10); //47999
                GPTimerCC26XX_setLoadValue(hTimer, loadVal);
                GPTimerCC26XX_registerInterrupt(hTimer, timerCallback, GPT_INT_TIMEOUT);
                GPTimerCC26XX_start(hTimer);
            }
            else if(state==TWO_KEY) {
                digit_1++;
                digit_1 = digit_1%10;
                PinCode = (digit_3*1000)+(digit_2*100)+(digit_1*10)+(digit_0*1);
                state = NO_KEY;

                GPTimerCC26XX_stop(hTimer);
                GPTimerCC26XX_Value loadVal = (TIMER_LOAD_VALUE_100MS*10); //47999
                GPTimerCC26XX_setLoadValue(hTimer, loadVal);
                GPTimerCC26XX_registerInterrupt(hTimer, timerCallback, GPT_INT_TIMEOUT);
                GPTimerCC26XX_start(hTimer);
            }
            else if(state==THREE_KEY) {
                digit_2++;
                digit_2 = digit_2%10;
                PinCode = (digit_3*1000)+(digit_2*100)+(digit_1*10)+(digit_0*1);
                state = NO_KEY;

                GPTimerCC26XX_stop(hTimer);
                GPTimerCC26XX_Value loadVal = (TIMER_LOAD_VALUE_100MS*10); //47999
                GPTimerCC26XX_setLoadValue(hTimer, loadVal);
                GPTimerCC26XX_registerInterrupt(hTimer, timerCallback, GPT_INT_TIMEOUT);
                GPTimerCC26XX_start(hTimer);
            }
            else if(state==FOUR_KEY) {
                digit_3++;
                digit_3 = digit_3%10;
                PinCode = (digit_3*1000)+(digit_2*100)+(digit_1*10)+(digit_0*1);
                state = NO_KEY;

                GPTimerCC26XX_stop(hTimer);
                GPTimerCC26XX_Value loadVal = (TIMER_LOAD_VALUE_100MS*10); //47999
                GPTimerCC26XX_setLoadValue(hTimer, loadVal);
                GPTimerCC26XX_registerInterrupt(hTimer, timerCallback, GPT_INT_TIMEOUT);
                GPTimerCC26XX_start(hTimer);
            }
            else if(state==OK_KEY) {
                system_state = QTY_ENTRY;
                Quantity = 0;
                state = NO_KEY;
                GPTimerCC26XX_stop(hTimer);


                /*
                 * RF UPDATE
                 */
                /*
                 * EPD UPDATE
                 */
                UpdateEPD("SENSOR","USD120/KG","PIN AUTH..");
                sleep(5);
                UpdateEPD("SENSOR","USD120/KG","ENTER QTY");
            }
            write_SevenSegment_Data(PinCode);
        }
        else if(system_state==QTY_ENTRY) {
            if(state==PLUS_KEY) {
                if(Quantity!=10000)
                    Quantity++;
                state = NO_KEY;
            }
            else if(state==MINUS_KEY) {
                if(Quantity!=0)
                    Quantity--;
                state = NO_KEY;
            }
            else if(state==OK_KEY) {
                system_state = PIN_ENTRY;
                digit_0 = 0;
                digit_1 = 0;
                digit_2 = 0;
                digit_3 = 0;
                PinCode = 0;
                state = NO_KEY;

                /*
                 * RF UPDATE
                 */
                /*
                 * EPD UPDATE
                 */
                //UpdateEPD("PIN ENTRY");
                UpdateEPD("SENSOR","TI15.4","Receiving");
                sleep(5);
                UpdateEPD("SENSOR","TI15.4","Receiving");
            }
            write_SevenSegment_Data(Quantity);
        }
    }
}
void UpdateEPD(char *Message1,char *Message2,char *Message3)
{
    Paint_Clear(&paint_black, UNCOLORED);
    Paint_Clear(&paint_red, UNCOLORED);
    Paint_DrawStringAt(&paint_red, 15,25, Message1, &Font24, COLORED);
    Paint_DrawStringAt(&paint_red, 10,55,Message2, &Font20, COLORED);
    Paint_DrawStringAt(&paint_red, 25,80,Message3, &Font16, COLORED);
    Paint_DrawStringAt(&paint_red, 12,115,"TONETAG",&Font24, COLORED);
    EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);
    EPD_Sleep(&epd);
    sleep(2);
}
void write_SevenSegment_Data(unsigned int value)
{
    if(value>9999) {
        return;
    }
    display_digit(3,((value/1)%10));
    display_digit(2,((value/10)%10));
    display_digit(1,((value/100)%10));
    display_digit(0,((value/1000)%10));
}
void display_digit(unsigned char pos, unsigned int value)
{
    switch(pos) {
    case 0:
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_22,0);
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_23,1);
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_24,1);
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_26,1);
        break;
    case 1:
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_22,1);
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_23,0);
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_24,1);
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_26,1);
        break;
    case 2:
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_22,1);
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_23,1);
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_24,0);
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_26,1);
        break;
    case 3:
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_22,1);
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_23,1);
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_24,1);
        PIN_setOutputValue(SevenSegDispPinHandle,IOID_26,0);
        break;
    }
    PIN_setOutputValue(SevenSegDispPinHandle,IOID_18,((value/1)%2));
    PIN_setOutputValue(SevenSegDispPinHandle,IOID_19,((value/2)%2));
    PIN_setOutputValue(SevenSegDispPinHandle,IOID_20,((value/4)%2));
    PIN_setOutputValue(SevenSegDispPinHandle,IOID_21,((value/8)%2));
    usleep(1);
    PIN_setOutputValue(SevenSegDispPinHandle,IOID_22,1);
    PIN_setOutputValue(SevenSegDispPinHandle,IOID_23,1);
    PIN_setOutputValue(SevenSegDispPinHandle,IOID_24,1);
    PIN_setOutputValue(SevenSegDispPinHandle,IOID_26,1);
}
