/**
 *  @filename   :   epdif.c
 *  @brief      :   Implements EPD interface functions
 *                  Users have to implement all the functions in epdif.cpp
 *  @author     :   Yehui from Waveshare
 *
 *  Copyright (C) Waveshare     July 7 2017
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documnetation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to  whom the Software is
 * furished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "epdif.h"
//#include "main.h"

//extern SPI_HandleTypeDef hspi1;

EPD_Pin epd_cs_pin = {
     DIO07_CS,
};

EPD_Pin epd_rst_pin = {
     DIO14_RES,
};

EPD_Pin epd_dc_pin = {
     DIO15_DC,
};

EPD_Pin epd_busy_pin = {
     DIO13_BSY,
};

EPD_Pin pins[4];



void EpdDigitalWriteCallback(int pin_num, int value) {
    switch(pin_num) {
    case 0: //CS-PIN
        if (value == HIGH)
            PIN_setOutputValue(GPIOPinHandle,IOID_7,1);
        else if(value == LOW)
            PIN_setOutputValue(GPIOPinHandle,IOID_7,0);
        break;
    case 1: //RST-PIN
        if (value == HIGH)
            PIN_setOutputValue(GPIOPinHandle,IOID_14,1);
        else if(value == LOW)
            PIN_setOutputValue(GPIOPinHandle,IOID_14,0);
        break;
    case 2: //DC-PIN
        if (value == HIGH)
            PIN_setOutputValue(GPIOPinHandle,IOID_15,1);
        else if(value == LOW)
            PIN_setOutputValue(GPIOPinHandle,IOID_15,0);
        break;
    case 4:
        if (value == HIGH)
            PIN_setOutputValue(GPIOPinHandle,IOID_6,1);
        else if(value == LOW)
            PIN_setOutputValue(GPIOPinHandle,IOID_6,0);
        break;
    }
#if 0
  if (value == HIGH) {
      //PIN_setOutputValue(GPIOPinHandle,pins[pin_num].pin,1);
      PIN_setOutputValue(GPIOPinHandle,pins[pin_num].pin,1);
    //HAL_GPIO_WritePin((GPIO_TypeDef*)pins[pin_num].port, pins[pin_num].pin, GPIO_PIN_SET);
  } else {
      PIN_setOutputValue(GPIOPinHandle,pins[pin_num].pin,0);
    //HAL_GPIO_WritePin((GPIO_TypeDef*)pins[pin_num].port, pins[pin_num].pin, GPIO_PIN_RESET);
  }
#endif
}

int EpdDigitalReadCallback(int pin_num) {
  //if (HAL_GPIO_ReadPin(pins[pin_num].port, pins[pin_num].pin) == GPIO_PIN_SET) {
  //if(PIN_getOutputValue(pins[pin_num].pin) == 1) {
    if(PIN_getOutputValue(IOID_13) == 1) {
    return HIGH;
  } else {
    return LOW;
  }
}

void EpdDelayMsCallback(unsigned int delaytime) {
  usleep(delaytime*1000);
    //sleep(1);
}

void SPI_Transmit(unsigned char data)
{
    unsigned char TEMPCOM = 0;
    unsigned char scnt = 0;

    TEMPCOM=data;
    SCLK_H;
    for(scnt=0;scnt<8;scnt++) {
        if(TEMPCOM&0x80)
            SDA_H;
        else
            SDA_L;
        SCLK_L;
        SCLK_H;
        TEMPCOM=TEMPCOM<<1;
    }
}
void EpdSpiTransferCallback(unsigned char data) {
    PIN_setOutputValue(GPIOPinHandle,DIO07_CS,0);
  //HAL_GPIO_WritePin((GPIO_TypeDef*)pins[CS_PIN].port, pins[CS_PIN].pin, GPIO_PIN_RESET);
  //HAL_SPI_Transmit(&hspi1, &data, 1, 1000);
    SPI_Transmit(data);
  PIN_setOutputValue(GPIOPinHandle,DIO07_CS,1);
  //HAL_GPIO_WritePin((GPIO_TypeDef*)pins[CS_PIN].port, pins[CS_PIN].pin, GPIO_PIN_SET);
}
PIN_Config GPIOPinTable[] = {
    DIO10_SCLK | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    DIO09_MOSI | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    DIO07_CS | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    DIO15_DC | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    DIO14_RES | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    //DIO06_TEST | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    DIO13_BSY | PIN_INPUT_EN | PIN_PULLDOWN,
    PIN_TERMINATE
};

int EpdInitCallback(void) {
  pins[CS_PIN] = epd_cs_pin;
  pins[RST_PIN] = epd_rst_pin;
  pins[DC_PIN] = epd_dc_pin;
  pins[BUSY_PIN] = epd_busy_pin;


  GPIOPinHandle = PIN_open(&GPIOPinState, GPIOPinTable);
  if(!GPIOPinHandle) {
      /* Error initializing board LED pins */
      while(1);
  }
  return 0;
}

