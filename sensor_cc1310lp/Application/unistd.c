/*
 * unistd.c
 *
 *  Created on: 23-Nov-2018
 *      Author: gaurav
 */


#include <xdc/std.h>

#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/drivers/PIN.h>


unsigned sleep(unsigned seconds)
{
    Task_sleep(seconds*100000);
    return 1;

}

int usleep(unsigned useconds){

    Task_sleep(useconds*0.1);
return 1;

}
